# Experimental CUDA project for learnning
## Building instructions
Steps
  - Check out from gitlab: `git clone https://gitlab.com/amartya00/experimentservice.git`
  - Navigate to the project folder: `cd experimentservice/cudaproject`
  - Make a out of tree build folder: `mkdir build_ && cd build_`
  - Build: `cmake .. && make`
    - If you get this error: `unsupported GNU version!`, use the compiler packaged with cuda toolkit when running cmake.
    - In Ubuntu, this works if you have installed CUDA from the repositories: 
      - `cmake -DCMAKE_C_COMPILER=/usr/lib/nvidia-cuda-toolkit/bin/gcc -DCMAKE_CXX_COMPILER=/usr/lib/nvidia-cuda-toolkit/bin/g++ ..`
    
