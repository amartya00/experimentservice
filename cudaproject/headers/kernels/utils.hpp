#ifndef __SIGABRT_CUDA_UTILS__
#define __SIGABRT_CUDA_UTILS__

// Test function
namespace Sigabrt {
    namespace Cuda {
        void cudaHello(const std::size_t&, const std::size_t&);
    }
}

#endif
