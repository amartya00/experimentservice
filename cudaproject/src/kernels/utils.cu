#include <iostream>
#include <kernels/utils.hpp>

__global__ void helloWorld(){
    std::printf("Hello World from GPU!\n");
}

void Sigabrt::Cuda::cudaHello(const std::size_t& groups, const std::size_t& cores) {
    std::cout << "Executing hello world cuda kernel\n";
    helloWorld<<<groups, cores>>>();
}
