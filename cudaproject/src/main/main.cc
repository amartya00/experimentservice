#include <iostream>
#include <kernels/utils.hpp>

using Sigabrt::Cuda::cudaHello;

int main() {
    std::cout << "Hello world!\n";
    cudaHello(1,3);
    return 0;
}
