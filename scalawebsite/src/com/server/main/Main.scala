package com.server.main

import com.google.inject.Module
import com.server.activities.modules.{
  HomeControllerModuleLocal,
  HomeControllerModuleS3,
  NotFoundControllerModuleLocal,
  NotFoundControllerModuleS3
}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.filters.{CommonFilters, LoggingMDCFilter, TraceIdMDCFilter}
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.routing.HttpRouter
import server.activities.controllers.{HomeController, NotFoundController}

/**
  * This is the server implementation with S3 based content loader.
  */
class FEServerS3 extends HttpServer {
  override def modules: Seq[Module] = Seq(HomeControllerModuleS3, NotFoundControllerModuleS3)

  override protected def configureHttp(router: HttpRouter): Unit = {
    router
      .filter[LoggingMDCFilter[Request, Response]]
      .filter[TraceIdMDCFilter[Request, Response]]
      .filter[CommonFilters]
      .add[HomeController]
      .add[NotFoundController]
  }

  override def failfastOnFlagsNotParsed: Boolean = true
}

/**
  * This is the server implementation with local content loader.
  */
class FEServerLocal extends HttpServer {
  override def modules: Seq[Module] = Seq(HomeControllerModuleLocal, NotFoundControllerModuleLocal)

  override protected def configureHttp(router: HttpRouter): Unit = {
    router
      .filter[LoggingMDCFilter[Request, Response]]
      .filter[TraceIdMDCFilter[Request, Response]]
      .filter[CommonFilters]
      .add[HomeController]
      .add[NotFoundController]
  }

  override def failfastOnFlagsNotParsed: Boolean = true
}

object FEServerS3Main extends FEServerS3

object FEServerLocalMain extends FEServerLocal

