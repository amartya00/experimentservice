package server.utils

/**
  * This is the implementation of a simple template renderer. Nothing too fancy loke moustache, just with basic functionality
  * of search replace {{**}}
  */
object SimpleTemplateRenderer {
  def render(html: String, values: Map[String, String]): String = {
    // TODO: Use string builder
    var temp = html // This is till I a better solution
    values.map { case(k, v) =>
      temp = temp.replaceAll(s"\\{\\{$k\\}\\}", s"$v")
    }
    temp
  }
}
