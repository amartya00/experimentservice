package com.server.utils

import java.util.concurrent.atomic.AtomicReference
import com.twitter.util.Try
import play.api.libs.json.Json

import scala.io.Source

object Defaults {
  val DefaultPagesConfig: Map[String, String] = Map(
    "/html/index.html" -> "*",
    "/html/notfound.html" -> "*"
  )
}

/**
  * This class is an encapsulation around the settings needed for the service to run properly. The config will be loaded
  * from `/etc/personalsite/config.json`
  *
  * @param stage
  *              Stage example alpha, beta, prod, etc
  * @param pages
  *              This contains a map of allowed pages vs their MD5 values for validation. This is to restrict
  *              loading of unwanted pages.
  */

case class PageConfigWithDefault(
  configPath: String,
  defaultConfig: Map[String, String],
  refreshInterval: Int
) {
  private var pages = new AtomicReference[Map[String, String]](defaultConfig)

  /**
    * This method swaps the page map atomically periodically with the loadd contents.
    */
  def refresh(): Unit = {
    println("Loading pages config.")
    val loadedConfig = Try {
      val contents = Source.fromFile(configPath).getLines().map(line => s"${line} \n").toSeq.mkString
      Json.parse(contents).as[Map[String, String]]
    }
      .getOrElse(defaultConfig)
    this.synchronized {
      pages.set(loadedConfig)
    }
  }

  def getPageConfig(): AtomicReference[Map[String, String]] = pages
}

