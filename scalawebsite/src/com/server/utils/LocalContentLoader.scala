package com.server.utils

import com.twitter.util.Try
import scala.io.Source

class LocalContentLoader(
  ep: String
) extends ContentLoader(ep) {
  // This function is used internally to loaf all the pages at startup.
  override def loadFile(filePath: String): String = {
    Try {
      val f = getClass.getResourceAsStream(filePath)
      Source.fromInputStream(f).getLines.mkString
    }
      .onFailure {exc =>
        throw new Exception(s"Failed to load file $filePath because ${exc.getMessage}")
      }.get()
    // We expect all pages to be present at startup. Hence
    // a failure to pre-load a page at startup should be unrecoverable error
  }

  override def refresh(): Unit = {

  }
}
