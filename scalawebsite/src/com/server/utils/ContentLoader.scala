package com.server.utils

import server.utils.SimpleTemplateRenderer

/**
  * This class is an abstract DAO for loading webpages. It has some pre defined translation methods that will be common
  * to all implementations. The implementations will have to define thd methods that do the actual fetch. The DAO is supposed
  * to load all pages in memory at startup and refresh thereafter. Hence the `pages` map, which contains the actual webpage
  * content is not lazy val.
  */
abstract class ContentLoader(ep: String) {
  // Page list
  protected val Index = "index.html"
  protected val NotFound = "notfound.html"

  // TODO: Move this to config
  // Parameter list
  protected val parameters: Map[String, String] = Map(
    "me" -> "Amartya",
    "endpoint" -> ep
  )

  //This map contains path tp webpage mappings. The loadFile needs to be implemented in the DAO implementations.
  protected val pages: Map[String, String] = Map(
    "/" -> SimpleTemplateRenderer.render(loadFile(translatePath(Index)), parameters),
    "/index.html" -> SimpleTemplateRenderer.render(loadFile(translatePath(Index)), parameters),
    "/notfound.html" -> SimpleTemplateRenderer.render(loadFile(translatePath(NotFound)), parameters)
  )

  // This method does some path translations before passing the result onto the load file method that will be implementation specific
  protected def translatePath(fileName: String): String = {
    fileName.split("\\.").last match {
      case "html" => s"/html/$fileName"
      case "jpg" => s"/images/$fileName"
      case "png" => s"/images/$fileName"
      case "ico" => s"/images/$fileName"
      case "js" => s"/js/$fileName"
      case _ => throw new Exception("Invalid file extension")
    }
  }

  def get(name: String): Option[String] = pages.get(name)
  def notFound(): String =  pages.get("/notfound.html").get

  // Methods that will be implementation specific
  def refresh(): Unit
  def loadFile(fileName: String): String
}
