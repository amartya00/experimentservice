package com.server.utils

import java.nio.charset.StandardCharsets

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.{GetObjectRequest, S3Object}
import com.twitter.util.Try
import org.apache.commons.io.IOUtils

/**
  * This class implements content loader for contents stored in S3. The client passed as input should take care of
  * retry, auth, etc.
  *
  * @param s3Client
  * @param bucketName
  */
class S3ContentLoader(
  ep: String,
  s3Client: AmazonS3,
  bucketName: String,
  prefix: String,
  pagesConfig: PageConfigWithDefault
) extends ContentLoader(ep) {
  // This function removes the leading `/` to make it compatable as an S3 key
  private def translatePaths(path: String): String = {
    val normalizedPath = path.head match {
      case '/' => path.replaceFirst("/", "")
      case _ => path
    }
    prefix match {
      case "" => normalizedPath
      case _ => s"$prefix/$normalizedPath"
    }
  }

  /**
    * This is the implementation specific function. This does a direct fetch from S3
    *
    * @param filePath
    * @return
    */
  override def loadFile(filePath: String): String = {
    Try {
      pagesConfig.getPageConfig().get().get(filePath).map { storedMd5 =>
        val getObjectResponse: S3Object = s3Client.getObject(
          new GetObjectRequest(
            bucketName, translatePaths(filePath)
          )
        )
        val retval = new String(IOUtils.toByteArray(getObjectResponse.getObjectContent()), StandardCharsets.UTF_8)
        val computerHash = HashUtils.md5HashString(retval)
        if (storedMd5 != "*" && computerHash != storedMd5) {
          throw new Exception(s"Computed md5 hash of ${filePath} (${computerHash}) did not match the stored hash ($storedMd5)")
        }
        retval
      }.getOrElse(throw new Exception(s"Could not find path ${translatePaths(filePath)} in the stored md5 file."))
    }.onFailure{exc =>
      throw new Exception(s"Failed to load file ${translatePaths(filePath)} from S3 bucket ${bucketName} because ${exc.getMessage()}")
    }.get
  }

  override def refresh(): Unit = {

  }
}

