package com.server.activities.modules

import com.google.inject.{Provides, Singleton}
import com.server.utils.{LocalContentLoader, S3ContentLoader}
import com.twitter.inject.TwitterModule
import server.activities.controllers.HomeController


object HomeControllerModuleS3 extends TwitterModule {
  override val modules = Seq(S3ContentLoaderModule)

  @Provides
  @Singleton
  def getHomeHandlerS3(contentLoader: S3ContentLoader): HomeController = {
    new HomeController(contentLoader)
  }
}

object HomeControllerModuleLocal extends TwitterModule {
  override val modules = Seq(LocalContentLoaderModule)

  @Provides
  @Singleton
  def getHomeHandlerLocal(contentLoader: LocalContentLoader): HomeController = {
    new HomeController(contentLoader)
  }
}
