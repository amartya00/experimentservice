package com.server.activities.modules

import com.google.inject.{Provides, Singleton}
import com.server.utils.{LocalContentLoader, S3ContentLoader}
import com.twitter.inject.TwitterModule
import server.activities.controllers.NotFoundController


object NotFoundControllerModuleS3 extends TwitterModule {
  override val modules = Seq(S3ContentLoaderModule)

  @Provides
  @Singleton
  def getHomeHandlerS3(contentLoader: S3ContentLoader): NotFoundController = {
    new NotFoundController(contentLoader)
  }
}

object NotFoundControllerModuleLocal extends TwitterModule {
  override val modules = Seq(LocalContentLoaderModule)

  @Provides
  @Singleton
  def getHomeHandlerS3(contentLoader: LocalContentLoader): NotFoundController = {
    new NotFoundController(contentLoader)
  }
}