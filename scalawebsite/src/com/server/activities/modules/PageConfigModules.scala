package server.activities.modules

import com.google.inject.{Provides, Singleton}
import com.server.utils.{Defaults, PageConfigWithDefault}
import com.twitter.inject.TwitterModule

object PageConfigModules extends TwitterModule {
  val configPath = flag[String]("config.pages", "The path to the pages config file")
  val refreshInterval = flag[Int]("config.pages.refresh", 60000, "The interval at which to refresh the pages config.")

  @Provides
  @Singleton
  def getPagesConfig(): PageConfigWithDefault = {
    configPath.get.map {path =>
      PageConfigWithDefault(
        configPath = configPath(),
        defaultConfig = Defaults.DefaultPagesConfig,
        refreshInterval =refreshInterval()
      )
    }.getOrElse {
      throw new Exception("Missing flag `config.pages`. Please specify that in the command line.")
    }

  }
}
