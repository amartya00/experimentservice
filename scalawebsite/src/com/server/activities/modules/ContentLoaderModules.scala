package com.server.activities.modules

import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.google.inject.{Module, Provides, Singleton}
import com.server.utils.{LocalContentLoader, PageConfigWithDefault, S3ContentLoader}
import com.twitter.inject.TwitterModule
import server.activities.modules.PageConfigModules

object S3ContentLoaderModule extends TwitterModule {
  override def modules: Seq[Module] = Seq(PageConfigModules)

  val bucketNameFlag = flag[String]("aws.s3.bucket", "Name of the S3 bucket to use")
  val prefixFlag = flag[String]("aws.s3.prefix", "S3 file prefix.")
  val endpointFlag = flag[String]("service.endpoint", "Service endpoint eg `abc.com` or `localhost`.")

  @Provides
  @Singleton
  def getS3ContentLoader(pagesConfig: PageConfigWithDefault): S3ContentLoader = {
    val s3Client: AmazonS3 = AmazonS3ClientBuilder.defaultClient()
    val bucketName = bucketNameFlag.get match {
      case None => throw new Exception(s"Flag `${bucketNameFlag.name}` not defined.")
      case Some(bucketNameStr) => bucketNameStr
    }

    val prefix = prefixFlag.get match {
      case None => throw new Exception(s"Flag `${prefixFlag.name}` not defined.")
      case Some(prefixStr) => prefixStr
    }

    val endpoint = endpointFlag.get match {
      case None => throw new Exception(s"Flag `${endpointFlag.name}` not defined.")
      case Some(ep) => ep
    }

    new S3ContentLoader(ep = endpoint, s3Client = s3Client, bucketName = bucketName, prefix = prefix, pagesConfig = pagesConfig)
  }
}

object LocalContentLoaderModule extends TwitterModule {
  val endpointFlag = flag[String]("service.endpoint", "Service endpoint eg `abc.com` or `localhost`.")

  @Provides
  @Singleton
  def getS3ContentLoader(): LocalContentLoader = {
    val endpoint = endpointFlag.get match {
      case None => throw new Exception(s"Flag `${endpointFlag.name}` not defined.")
      case Some(ep) => ep
    }

    new LocalContentLoader(ep = endpoint)
  }
}