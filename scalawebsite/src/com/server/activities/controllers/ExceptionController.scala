package server.activities.controllers

import com.server.utils.ContentLoader


class NotFoundController(contentLoader: ContentLoader) extends BaseController(contentLoader) {
  val Path: String = "/:*"
  get(Path)(doNotFound(Path, _))
  post(Path)(doNotFound(Path, _))
}
