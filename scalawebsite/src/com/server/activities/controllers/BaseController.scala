package server.activities.controllers

import com.server.utils.ContentLoader
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.finatra.http.Controller
import com.twitter.io.Buf
import com.twitter.util.Future

/**
  * This class implements the controller wiring where it just does a simple fetch from the content loader and returns a
  * response. Specific controllers can extend this and register their own routes using the `do*` functions defined here.
  * @param contentLoader
  */
abstract class BaseController(
  contentLoader: ContentLoader
) extends  Controller {
  def doGet(path: String, request: Request):Future[Option[Response]] = {
    info(s"GET request for ${path} from ${request.remoteAddress}")

    Future.value(contentLoader.get(path).map {
      str =>
        val response = Response(Status.Ok).content(Buf.Utf8(str))
        response.headerMap.add("Content-Type", "text/html;charset=UTF-8")
        response
    })
  }

  def doPost(path: String, request: Request):Future[Option[Response]] = {
    info(s"POST request for ${path} from ${request.remoteAddress}")

    Future.value(contentLoader.get(path).map {
      str =>
        val response = Response(Status.Ok).content(Buf.Utf8(str))
        response.headerMap.add("Content-Type", "text/html;charset=UTF-8")
        response
    })
  }

  def doNotFound(path: String, request: Request): Future[Option[Response]] = {
    debug(s"Not found ${path}.")
    val resp: Response = Response(Status.NotFound).content(Buf.Utf8(contentLoader.notFound()))
    resp.headerMap.add("Content-Type", "text/html;charset=UTF-8")
    Future.value(Some(resp))
  }
}
