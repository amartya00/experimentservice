package server.activities.controllers


import com.server.utils.ContentLoader

class HomeController(contentLoader: ContentLoader) extends BaseController(contentLoader) {
  info("Creating home controller")

  val Path: String = "/index.html"
  val Path2: String = "/"

  get(Path)(doGet(Path, _))
  post(Path)(doPost(Path, _))
  get(Path2)(doGet(Path2, _))
  post(Path2)(doPost(Path2, _))
}
