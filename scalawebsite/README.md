# Personal website
I created this small website to invest in learnning some popular open source technologies like:
  - Finatra
  - Scala
  - Pants
  
## How to build and run
  - Build: `./pants bundle src/com/server/main:bundlelocal`
  - Test run: `java -jar dist/server.jar -http.port=:8080 -service.endpoint=localhost`
  - Run: `java -jar dist/server.jar -http.port=:8080 -service.endpoint=localhost -aws.s3.bucket=<S3 bucket> -aws.s3.prefix=<Folder prefix>`