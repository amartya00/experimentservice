#include <algorithm>
#include <memory>
#include <thread>
#include <cstdio>
#include <chrono>

#include <ds/vector.hpp>
#include <testing/benchmark.hpp>

#include <folly/io/async/EventBase.h>
#include <folly/io/async/EventHandler.h>
#include <folly/io/async/EventBaseManager.h>
#include <folly/executors/GlobalExecutor.h>
#include <folly/executors/CPUThreadPoolExecutor.h>
#include <folly/executors/IOThreadPoolExecutor.h>
#include <folly/init/Init.h>
#include <folly/futures/Future.h>

#include <HTML/HTML.h>


using Sigabrt::Testing::Benchmark;
using Sigabrt::Testing::randomInputGenerator;
using Sigabrt::DataStructures::Vector;

using folly::EventHandler;
using folly::EventBase;
using folly::EventBaseManager;
using folly::Executor;
using folly::CPUThreadPoolExecutor;
using folly::IOThreadPoolExecutor;
using folly::Future;

int delayedExecution(const int& id) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::printf("Running the delayed function\n");
    return id;
}

int main(int argc, char* argv[]) {
    /*HTML::Document document {"Test document."};
    document.addAttribute("lang", "en");
    document.head() 
        << HTML::Meta("utf-8")
        << HTML::Meta("viewport", "width=device-width, initial-scale=1, shrink-to-fit=no")
        << HTML::Rel("stylesheet", "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css")
            .integrity("sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T").crossorigin("anonymous")
        << HTML::Style(".navbar{margin-bottom:20px;}");
    document.body().cls("bg-light")
        << HTML::Header1("This is stuff") << HTML::Paragraph("Potty");
    
    std::cout << document << "\n";*/

    folly::init(&argc, &argv);
    //std::shared_ptr<Executor> cpuExecutor = folly::getCPUExecutor();
    IOThreadPoolExecutor cpuExecutor {4};
    std::cout << cpuExecutor.numThreads() << "\n";
    
    std::vector<Future<int>> futures;
    
    for (int i = 0; i < 10; i++) {
        futures.push_back(folly::via(Executor::getKeepAliveToken(&cpuExecutor), std::bind(delayedExecution, i)));
    }
    Future<std::vector<int>> allFutures = folly::collect(futures);
    
    allFutures.wait();
    for (auto i: allFutures.value()) {
        std::cout << i << "\n";
    }
    
    //std::unique_ptr<EventBase> base {cpuExecutor-> getEventBase()};
    
}
