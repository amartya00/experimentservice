#ifndef __SIGABRT_TESTING_BENCHMARK__
#define __SIGABRT_TESTING_BENCHMARK__

#include <thread>
#include <future>
#include <chrono>
#include <functional>
#include <vector>
#include <fstream>
#include <exception>
#include <string>
#include <type_traits>
#include <cstddef>
#include <random>
#include <iostream>

using std::size_t;

namespace Sigabrt {
    namespace Testing {
        /**
         * This class can take a function from the user and profile it.
         * It can run it on multiple threads in loops and report back the 
         * results as well as profiling information. 
        */
        template <typename Ret, typename ArgType> class Benchmark {
        private:
            std::function<Ret(ArgType)> call;
            std::function<ArgType()> inputGenerator;
        public:
            Benchmark(std::function<Ret(ArgType)> call, std::function<ArgType()> inputGenerator): call {call}, inputGenerator {inputGenerator} {}
            std::vector<double> benchmarkWithoutResults(
                const unsigned long& numThreads,
                const unsigned long& callsPerThread
            ) {
                // This lambda will run the call() callsPerThread number of times sequentially
                // and return the runtime per call.
                auto f = [&]() -> double {
                    std::chrono::time_point start = std::chrono::high_resolution_clock::now();
                    for (unsigned long j = 0; j < callsPerThread; j++) {
                        ArgType arg = inputGenerator();
                        call(arg);
                    }
                    std::chrono::time_point end = std::chrono::high_resolution_clock::now();
                    auto durationInMicros = std::chrono::duration_cast<std::chrono::microseconds>(end-start);
                    return ((double)durationInMicros.count() / (double)callsPerThread);
                };

                // Asynchronously call the threads and save the times.
                std::vector<std::future<double>> threads {numThreads};
                std::vector<double> times {};
                for (unsigned long i = 0; i < numThreads; i++) {
                    threads[i] = std::async(f);
                }
                for (auto& thd : threads) {
                    times.push_back(thd.get());
                }
                return times;
            }
        };

        /**
         * Utility function to generate inputs frpom a file.
         * **/
        template <typename T> std::vector<T> fileInputGenerator(const std::string& filename) {
            std::ifstream in {filename};
            std::string line;
            std::vector<T> retval;
            if (in.is_open()) {
                while(std::getline(in, line)) {
                    try {
                        retval.push_back(static_cast<T>(std::stod(line)));
                    } catch (const std::exception& e) {
                        std::cout << "[WARN] Cound not convert " << line << " to double because " << e.what() << "\n";
                    }
                }
                in.close();
            } else {
                throw std::runtime_error("Failed to open file");
            }
            return retval;
        }

        /**
         * Utility function to generate a random numeric vector of a given size.
         * **/
        template <typename T> std::vector<T> randomInputGenerator(size_t size, const T& min, const T& max) {
            static_assert(std::is_floating_point<T>::value || std::is_integral<T>::value, "This function works only on numeric types. Sorry! :(");
            std::random_device rd;
            std::mt19937_64 mt {rd()};
            std::normal_distribution<double> dist(min, max);
            std::vector<T> retval;
            retval.reserve(size);
            for (size_t i = 0; i < size; i++) {
                retval.push_back(static_cast<T>(dist(mt)));
            }
            return retval;
        }
    }
}

#endif
