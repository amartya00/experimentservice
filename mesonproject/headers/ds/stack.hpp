#ifndef __SIGABRT_DS_STACK__
#define __SIGABRT_DS_STACK__

#include <cstddef>
#include <stdexcept>
#include <vector>
#include <type_traits>
#include <shared_mutex>

namespace Sigabrt {
    namespace DataStructures {
        constexpr static const char* OVERFLOW_ERROR {"Stack overflow."};
        constexpr static const char* UNDERFLOW_ERROR {"Stack underflow."};

        /**
         * Static stack implementation. This uses a static array to implement the stack.
         */
        template <typename T, std::size_t SZ> class StaticStack {
            static_assert(std::is_constructible<T>::value, "T has to be default constructible.");
        private:
            std::size_t head {0};
            T storage[SZ];
            mutable std::shared_mutex lock;
            
        public:
            const std::size_t size() const noexcept {
                return head; 
            }

            void push(const T& elem) {
                std::lock_guard<std::shared_mutex> guard {lock};
                if (head == SZ) {
                    throw std::overflow_error(OVERFLOW_ERROR);
                } else {
                    storage[head] = elem;
                    head++;
                }
            }

            template<typename ... Args> void emplace(const Args& ... args) {
                std::lock_guard<std::shared_mutex> guard {lock};
                if (head == SZ) {
                    throw std::overflow_error(OVERFLOW_ERROR);
                } else {
                    storage[head] = std::move(T(args...));
                    head++;
                }
            }

            T pop() {
                std::lock_guard<std::shared_mutex> guard {lock};
                if (head == 0) {
                    throw std::underflow_error(UNDERFLOW_ERROR);
                } else {
                    head--;
                    return storage[head];
                }
            }

            const T& peek() const {
                std::shared_lock<std::shared_mutex> guard {lock};
                if (head == 0) {
                    throw std::underflow_error(UNDERFLOW_ERROR);
                } else {
                    return storage[head-1];
                }
            }
        };

        /**
         * Dynamic stack implementation. This uses a std::vector as storage
         */
        template <typename T> class DynamicStack {
            static_assert(std::is_constructible<T>::value, "T has to be default constructible.");
        private:
            constexpr static const typename std::vector<T>::size_type INIT_SIZE = 10l;
            std::vector<T> store {};
            mutable std::shared_mutex lock;
            
        public:
            const std::size_t size() const noexcept {
                return store.size(); 
            }

            void push(const T& elem) {
                std::lock_guard<std::shared_mutex> guard {lock};
                store.push_back(elem);
            }

            template<typename ... Args> void emplace(const Args& ... args) {
                std::lock_guard<std::shared_mutex> guard {lock};
                store.emplace_back(args...);
            }

            T pop() {
                std::lock_guard<std::shared_mutex> guard {lock};
                if (store.size() == 0) {
                    throw std::underflow_error(UNDERFLOW_ERROR);
                } else {
                    T& retval = store.back();
                    store.pop_back();
                    return retval;
                }
            }

            const T& peek() const {
                std::shared_lock<std::shared_mutex> guard {lock};
                if (store.size() == 0) {
                    throw std::underflow_error(UNDERFLOW_ERROR);
                } else {
                    return store.back();
                }
            }
        };
    }  
} 

#endif
