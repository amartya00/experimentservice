#ifndef __SIGABRT_DS_Q__
#define __SIGABRT_DS_Q__

#include <atomic>
#include <cstddef>
#include <exception>
#include <list>
#include <optional>
#include <shared_mutex>

// TODO: Remove size() methods. Its pointless and in case of static Q, inaccurate.

namespace Sigabrt {
    namespace DataStructures {
        constexpr const char* OVERFLOW_ERROR {"Queue full."};

        template <typename T> struct AtomicStore {
            std::atomic_flag busy = ATOMIC_FLAG_INIT;
            T elem;
        };

        template <typename T, std::size_t SZ> class StaticCQueue {
        private:
            constexpr static const std::size_t STORAGE_SIZE = SZ+1;
            AtomicStore<T> storage[STORAGE_SIZE];
            std::size_t head {0};
            std::size_t tail {0};
            std::atomic<std::size_t> numel {0};
            std::atomic_flag qUpdateBusy = ATOMIC_FLAG_INIT;
        
        public:
            void enq(const T& elem) {
                // The Q state modification will need to happen serially.
                // Spin lock is being used here as this operation is very short.
                while(qUpdateBusy.test_and_set());
                if (tail - head == 1 || (tail == 0 && head == STORAGE_SIZE-1)) {
                    qUpdateBusy.clear();
                    throw std::overflow_error(OVERFLOW_ERROR);
                }
                std::size_t idx = (head+1) %(STORAGE_SIZE);
                if(storage[idx].busy.test_and_set()) {
                    // This indicates the tail is still being read.
                    // That is equivalent to a Q full, even though the tail is updated.
                    qUpdateBusy.clear();
                    throw std::overflow_error(OVERFLOW_ERROR);
                }
                numel++;
                // Now that the individual storage slot has been locked, update head and release the queue status lock
                head = idx;
                qUpdateBusy.clear();
                storage[idx].elem = elem;
                storage[idx].busy.clear();
            }

            std::optional<T> deq() noexcept {
                // The Q state modification will need to happen serially.
                // Spin lock is being used here as this operation is very short.
                while(qUpdateBusy.test_and_set());
                if (head == tail) {
                    qUpdateBusy.clear();
                    return std::nullopt;
                }
                std::size_t idx = (tail+1) % (STORAGE_SIZE);
                if (storage[idx].busy.test_and_set()) {
                    // This will indicate this storagearea is still being written to.
                    // Writing has not finished yet will indicate an empty Q.
                    qUpdateBusy.clear();
                    return std::nullopt;
                }
                numel--;
                // Now that the individual storage slot has been locked, update tail and release the queue status lock
                tail = idx;
                qUpdateBusy.clear();
                std::optional<T> retval {std::move(storage[idx].elem)};
                storage[idx].busy.clear();
                return retval;
            }

            std::size_t size() const noexcept {
                return numel.load();
            }
        };

        template <typename T> class DynamicQueue {
        private:
            std::list<T> store;
            mutable std::shared_mutex lock;
        public:
            void enq(const T& elem) noexcept {
                std::lock_guard<std::shared_mutex> guard {lock};
                store.push_back(elem);
            }

            std::optional<T> deq() noexcept {
                std::lock_guard<std::shared_mutex> guard {lock};
                if(store.size() == 0) {
                    return std::nullopt;
                }
                std::optional<T> retval {std::move(store.front())};
                store.pop_front();
                return retval;
            }

            std::size_t size() const noexcept {
                std::shared_lock<std::shared_mutex> guard {lock};
                return store.size();
            }
        };
    }
}

#endif
