#ifndef __SIGABRT_DS_VECTOR__
#define __SIGABRT_DS_VECTOR__

#include <cstddef>
#include <memory>
#include <cstdlib>
#include <exception>
#include <vector>

using std::size_t;

namespace Sigabrt {
    namespace DataStructures {
        constexpr const double TABLE_DOUBLING_FRACTON = 0.7;
        constexpr const double TABLE_HALVING_FRACTON = 0.2;

        // Move to making this atomics only
        template <typename T> class Vector {
        private:
            size_t capacity;
            size_t initCap;
            size_t numel {0};
            std::unique_ptr<T[]> handle;
        public:
            Vector(const size_t& initSize): capacity {initSize}, initCap {initSize}, handle {std::make_unique<T[]>(initSize)} {}
            Vector(const std::vector<T>& other): capacity {other.size()}, initCap {other.size()}, handle {std::make_unique<T[]>(other.size())} {
                size_t iter = 0;
                for (const auto& elem : other) {
                    handle.get()[iter++] = elem;
                }
                numel = other.size();
            }

            const size_t& size() const noexcept {
                return numel;
            }

            void push(const T& elem) noexcept {
                double fraction = static_cast<double>(numel) / static_cast<double>(capacity);
                
                if (fraction > TABLE_DOUBLING_FRACTON) {
                    T* newArr = static_cast<T*>(std::realloc(handle.get(), sizeof(T) * capacity * 2));
                    handle.release();
                    handle.reset(newArr);
                    capacity = capacity * 2;
                } else if (fraction < TABLE_HALVING_FRACTON && capacity > initCap) {
                    T* newArr = static_cast<T*>(std::realloc(handle.get(), sizeof(T) * (capacity/2)));
                    handle.release();
                    handle.reset(newArr);
                    capacity = capacity / 2;
                }
                handle.get()[numel] = elem;
                numel++;
            }

            T pop() {
                if (numel == 0) {
                    throw std::underflow_error("Vector is empty. Cannot pop!");
                }
                numel --;
                T retval = handle.get()[numel];
                return retval;
            }

            T last() {
                if (numel == 0) {
                    throw std::underflow_error("Vector is empty. Cannot pop!");
                }
                T retval = handle.get()[numel-1];
                return retval;
            }

            void put(const size_t& index, const T* elem) {
                if (index >= numel) {
                    throw std::invalid_argument("Cannot access element greater that vector's size!");
                }
                handle.get()[index] = elem;
            }

            T get(const size_t& index) const {
                if (index >= numel) {
                    throw std::invalid_argument("Cannot access element greater that vector's size!"); 
                }
                return handle.get()[index];
            }
        };
    }
}

#endif
