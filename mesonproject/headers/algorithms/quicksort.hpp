#ifndef _SIGABRT_ALGO_QUICKSORT_
#define _SIGABRT_ALGO_QUICKSORT_

#include <cstddef>
#include <vector>
#include <utility>
#include <iostream>
#include <random>
#include <algorithm>

using std::size_t;

namespace Sigabrt {
    namespace Algorithms {
        template <typename T> size_t partition(std::vector<T>& input, const size_t& start, const size_t end, const size_t pivot) {
            // Step 1: Bring the pivot element to the start of the subarray
            if(pivot != start) {
                std::swap(input[start], input[pivot]);
            }
            
            // Step 2: Run the for loop maintaining pivot invariant;
            auto i = start;
            for (auto j = i+1; j <=end; j++) {
                if (input[j] <= input[start]) {
                    std::swap(input[i+1], input[j]);
                    i++;
                }
            }
            std::swap(input[start], input[i]);
            return i;
        }

        template <typename T> void quickSortCallback(std::vector<T>& input, const size_t& start, const size_t& end, std::mt19937_64& rand) {
            if (start >= end) {
                return;
            } else if (end - start == 1) {
                if (input[end] < input[start]) {
                    std::swap(input[start], input[end]);
                }
            } else {
                // Chose pivot (random pivot)
                std::uniform_real_distribution<double> dist(static_cast<double>(start), static_cast<double>(end));
                size_t pivot = static_cast<size_t>(dist(rand));
                
                // Partition
                size_t newPivot = partition(input, start, end, pivot);
                
                // Recorse of left and right halves
                if (newPivot > start) quickSortCallback(input, start, newPivot-1, rand);
                if (newPivot < end) quickSortCallback(input, newPivot+1, end, rand);
            }
        }

        // TODO: Move to using iterators
        template <typename T> void quickSort(std::vector<T>& input) {
            std::random_device rd;
            std::mt19937_64 mt {rd()};
            quickSortCallback(input, 0, input.size()-1, mt);
        }
    }
}


#endif

