#ifndef _SIGABRT_ALGO_MERGESORT_
#define _SIGABRT_ALGO_MERGESORT_

#include <vector>
#include <cstddef>
#include <iostream>

using std::size_t;

namespace Sigabrt {
    namespace Algorithms {
        template <typename T> void merge(std::vector<T>& arr, const size_t& start, const size_t& mid, const size_t& end) {
            std::vector <T> temp {};
            temp.reserve(end - start + 1);
            
            size_t i = start;
            size_t j = mid+1;
            
            while(i <= mid && j <= end) {
                if (arr[i] <= arr[j]) {
                    temp.push_back(arr[i]);
                    i++;
                } else {
                    temp.push_back(arr[j]);
                    j++;
                }
            }
            if (i <= mid) {
                temp.insert(temp.end(), arr.begin() + i, arr.begin() + mid + 1);
            }
            if (j <= end) {
                temp.insert(temp.end(), arr.begin() + j, arr.begin() + end + 1);
            }
            auto it = arr.begin() + start;
            for (auto& elem : temp) {
                *it = elem;
                it++;
            }
        }
    
        template <typename T> void mergeSortCallback(std::vector<T>& input, const size_t& start, const size_t& end) {
            if(start >= end) return;
            size_t mid = (start + end)/2;
            mergeSortCallback(input, start, mid);
            mergeSortCallback(input, mid+1, end);
            merge(input, start, mid, end);
        }

        // TODO: Move to using iterators
        template <typename T> void mergeSort(std::vector<T>& input) {
            mergeSortCallback(input, 0, input.size()-1);
        }
    }
}

#endif
