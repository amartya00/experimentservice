#define CATCH_CONFIG_MAIN

#include <atomic>
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <thread>
#include <cstdlib>
#include <shared_mutex>

#include <testing/catch.hpp>
#include <testing/benchmark.hpp>

using Sigabrt::Testing::Benchmark;

SCENARIO("Test benchmark makes calls properly") {
    
    GIVEN("I have a setup to capture number of calls ") {
        constexpr const unsigned long NUM_THREADS = 10;
        constexpr const unsigned long CALLS_PER_THREAD = 20;

        std::atomic<int> numCalls {0};
        std::vector<int> inputsToUse {}; // This will contain the inputs that we will pass
        std::vector<int> passedArgs {}; // This will collect the passed args.

        // This lambda induces a small delay and increments the call counter
        std::shared_mutex capturedInputsLock;
        auto f = [&](const int& input) {
            // Induce a small sleep for simulated jitter
            std::this_thread::sleep_for(std::chrono::milliseconds(rand()%10));
            numCalls++;
            std::lock_guard<std::shared_mutex> guard {capturedInputsLock};
            passedArgs.push_back(input);
        };

        // This function reads from the input queue to generate inputs
        std::shared_mutex inputLock;
        auto inputGen = [&]() -> int {
            int input = (int) rand();
            std::lock_guard<std::shared_mutex> guard {inputLock};
            inputsToUse.push_back(input);
            return input;
        };

        WHEN("I run the benchmark.") {
            Benchmark<void, int> bm {f, inputGen};
            std::vector<double> times = bm.benchmarkWithoutResults(NUM_THREADS, CALLS_PER_THREAD);

            THEN("The number of durations should match the number of threads.") {
                REQUIRE(NUM_THREADS == times.size());

                AND_THEN("The number of calls made should equal the product of the number of threads and calls made ") {
                    REQUIRE(NUM_THREADS*CALLS_PER_THREAD == passedArgs.size());
                    REQUIRE(NUM_THREADS*CALLS_PER_THREAD == inputsToUse.size());

                    AND_THEN("All the inputs that were generated should have been passed into the function") {
                        std::sort(inputsToUse.begin(), inputsToUse.end());
                        std::sort(passedArgs.begin(), passedArgs.end());
                        REQUIRE(passedArgs == inputsToUse);
                    } 
                }
            }
        }
    }
}
