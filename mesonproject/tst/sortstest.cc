#define CATCH_CONFIG_MAIN

#include <algorithm>
#include <vector>
#include <algorithms/quicksort.hpp>
#include <algorithms/mergesort.hpp>
#include <testing/catch.hpp>
#include <testing/benchmark.hpp>

using Sigabrt::Algorithms::quickSort;
using Sigabrt::Algorithms::mergeSort;
using Sigabrt::Testing::randomInputGenerator;

SCENARIO("Testing quicksort.") {
    GIVEN("I have an unsorted array") {
        std::vector<double> sampleArr {randomInputGenerator<double>(10000, -1000.0, 1000.0)};
        std::vector<double> testArr {sampleArr};
        
        WHEN("I call quickSort on the sample array") {
            quickSort(sampleArr);
            std::sort(testArr.begin(), testArr.end());
            
            THEN("The array should get sorted") {
                REQUIRE(testArr == sampleArr);
            }
        }
    }
}

SCENARIO("Testing mergesort.") {
    GIVEN("I have an unsorted array") {
        std::vector<double> sampleArr {randomInputGenerator<double>(10000, -1000.0, 1000.0)};
        std::vector<double> testArr {sampleArr};
        
        WHEN("I call mergesort on the sample array") {
            mergeSort(sampleArr);
            std::sort(testArr.begin(), testArr.end());
            
            THEN("The array should get sorted") {
                REQUIRE(testArr == sampleArr);
            }
        }
    }
}
