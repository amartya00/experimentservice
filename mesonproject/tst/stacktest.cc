#define CATCH_CONFIG_MAIN

#include <exception>
#include <thread>
#include <ds/stack.hpp>
#include<testing/catch.hpp>

using Sigabrt::DataStructures::StaticStack;
using Sigabrt::DataStructures::DynamicStack;

SCENARIO("Static test happy case.") {
    GIVEN("I have a static stack of integer type.") {
        StaticStack<int, 3> stk {};

        WHEN("I push an element into the stack.") {
            stk.push(100);

            THEN("The stack size should be 1.") {
                REQUIRE(stk.size() == 1);
            }

            AND_THEN("The element at top of the stack should be correct.") {
                REQUIRE(stk.peek() == 100);
            }
        }
        
        AND_WHEN("I pop an elemnt from the stack.") {
            stk.push(100);
            int val = stk.pop();

            THEN("The stack size should be 0.") {
                REQUIRE(stk.size() == 0);
            }

            AND_THEN("The popped element should be correct.") {
                REQUIRE(100 == val);
            }
        }

        AND_WHEN("I push a series of elements and then pop them.") {
            stk.push(1);
            stk.push(2);
            stk.push(3);
            int i1 = stk.pop();
            int i2 = stk.pop();
            int i3 = stk.pop();

            THEN("The elements should be popped in reverse order in which they were put in.") {
                REQUIRE(3 == i1);
                REQUIRE(2 == i2);
                REQUIRE(1 == i3);
            }
        }
    }

}

SCENARIO("Dynamic test happy case.") {
    GIVEN("I have a static stack of integer type.") {
        DynamicStack<int> stk {};

        WHEN("I push an element into the stack") {
            stk.push(100);

            THEN("The stack size should be 1.") {
                REQUIRE(stk.size() == 1);
            }

            AND_THEN("The element at top of the stack should be correct") {
                REQUIRE(stk.peek() == 100);
            }
        }

        AND_WHEN("I pop an elemnt from the stack") {
            stk.push(100);
            int val = stk.pop();

            THEN("The stack size should be 0") {
                REQUIRE(stk.size() == 0);
            }

            AND_THEN("The popped element should be correct") {
                REQUIRE(100 == val);
            }
        }

        AND_WHEN("I push a series of elements and then pop them.") {
            stk.push(1);
            stk.push(2);
            stk.push(3);
            int i1 = stk.pop();
            int i2 = stk.pop();
            int i3 = stk.pop();

            THEN("The elements should be popped in reverse order in which they were put in.") {
                REQUIRE(3 == i1);
                REQUIRE(2 == i2);
                REQUIRE(1 == i3);
            }
        }
    }

}

SCENARIO("Test for exceptions in static stack") {
    GIVEN("I have a staticstack") {
        StaticStack<int, 2> stk {};

        WHEN("I do a peek.") {
            THEN("I should get an underflow error") {
                REQUIRE_THROWS_AS(stk.peek(), std::underflow_error);
            }
        }

        AND_WHEN("I do too many pushes.") {
            stk.push(1);
            stk.push(2);

            THEN("I should get an overflow error.") {
                REQUIRE_THROWS_AS(stk.push(3), std::overflow_error);
            }
        }
    }
}

SCENARIO("Test for exceptions in dynamic stack") {
    GIVEN("I have a staticstack") {
        DynamicStack<int> stk {};

        WHEN("I do a peek.") {
            THEN("I should get an underflow error") {
                REQUIRE_THROWS_AS(stk.peek(), std::underflow_error);
            }
        }
    }
}

SCENARIO("Static stack multi threaded tests.") {
    GIVEN("I have a static stack.") {
        constexpr static const std::size_t NUMEL = 10;
        StaticStack<int, NUMEL> stk;

        WHEN("I push elements into it from multiple threads") {
            auto push = [&stk]() {
                stk.push(100);
            };
            std::thread threads[NUMEL];
            for(size_t i = 0; i < NUMEL; i++) {
                threads[i] = std::thread(push);
            }

            for(auto& thd: threads) {
                thd.join();
            }

            THEN("The stack should contain all elements I pushed") {
                REQUIRE(NUMEL == stk.size());
            }
        }

        AND_WHEN("I push and pop elements into/from it from multiple threads") {
            auto push = [&stk]() {
                stk.push(100);
            };
            auto pop = [&stk]()->int {
                return stk.pop();
            };
            std::thread threads[NUMEL];
            //Push
            for(size_t i = 0; i < NUMEL; i++) threads[i] = std::thread(push);
            for(auto& thd: threads) thd.join();

            // Pop
            for(size_t i = 0; i < NUMEL; i++) threads[i] = std::thread(pop);
            for(auto& thd: threads) thd.join();

            THEN("The stack should contain all elements I pushed") {
                REQUIRE(0 == stk.size());
            }
        }
    }
}

SCENARIO("Dynamic stack multi threaded tests.") {
    GIVEN("I have a static stack.") {
        constexpr static const std::size_t NUMEL = 10;
        DynamicStack<int> stk;

        WHEN("I push elements into it from multiple threads") {
            auto push = [&stk]() {
                stk.push(100);
            };
            std::thread threads[NUMEL];
            for(size_t i = 0; i < NUMEL; i++) {
                threads[i] = std::thread(push);
            }

            for(auto& thd: threads) {
                thd.join();
            }

            THEN("The stack should contain all elements I pushed") {
                REQUIRE(NUMEL == stk.size());
            }
        }

        AND_WHEN("I push and pop elements into/from it from multiple threads") {
            auto push = [&stk]() {
                stk.push(100);
            };
            auto pop = [&stk]()->int {
                return stk.pop();
            };
            std::thread threads[NUMEL];
            //Push
            for(size_t i = 0; i < NUMEL; i++) threads[i] = std::thread(push);
            for(auto& thd: threads) thd.join();

            // Pop
            for(size_t i = 0; i < NUMEL; i++) threads[i] = std::thread(pop);
            for(auto& thd: threads) thd.join();

            THEN("The stack should contain all elements I pushed") {
                REQUIRE(0 == stk.size());
            }
        }
    }
}

SCENARIO("Test static stack emplace with complex structures.") {
    GIVEN("I have a complex structure") {

        struct Data {
            int x {};
            std::string y {""};
            Data() {}
            Data(const int& x, const std::string& y): x{x}, y{y} {}
        };

        AND_GIVEN("I have a static stack.") {
            StaticStack<Data, 5> myStack;

            WHEN("I do emplace.") {
                myStack.emplace(1, std::string("papaya"));
                myStack.emplace(2, std::string("mango"));

                THEN("It should work") {

                    Data d1 = myStack.pop();
                    Data d2 = myStack.pop();

                    REQUIRE(std::string("mango").compare(d1.y) == 0);
                    REQUIRE(std::string("papaya").compare(d2.y) == 0);
                }
            }
        }
    }
}

SCENARIO("Test dynamic stack emplace with complex structures.") {
    GIVEN("I have a complex structure") {

        struct Data {
            int x {};
            std::string y {""};
            Data() {}
            Data(const int& x, const std::string& y): x{x}, y{y} {}
        };

        AND_GIVEN("I have a dynamic stack.") {
            DynamicStack<Data> myStack;

            WHEN("I do emplace.") {
                myStack.emplace(1, std::string("papaya"));
                myStack.emplace(2, std::string("mango"));

                THEN("It should work") {

                    Data d1 = myStack.pop();
                    Data d2 = myStack.pop();

                    REQUIRE(std::string("mango").compare(d1.y) == 0);
                    REQUIRE(std::string("papaya").compare(d2.y) == 0);
                }
            }
        }
    }
}
