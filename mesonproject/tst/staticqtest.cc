#define CATCH_CONFIG_MAIN

#include <algorithm>
#include <cstdlib>
#include <cstddef>
#include <chrono>
#include <exception>
#include <mutex>
#include <optional>
#include <thread>

#include <testing/catch.hpp>
#include <testing/benchmark.hpp>
#include <ds/queue.hpp>

using Sigabrt::DataStructures::StaticCQueue;
using Sigabrt::Testing::Benchmark;

SCENARIO("Static circular queue happy case.") {

    GIVEN("I have a static circular Q.") {
        StaticCQueue<int, 3> q;

        WHEN("I insert 3 elements into the queue and extract 3 elements.") {
            q.enq(1);
            q.enq(2);
            q.enq(3);

            int i1 = *q.deq();
            int i2 = *q.deq();
            int i3 = *q.deq();

            THEN("The elements should come out in the order in which they were inserted.") {
                REQUIRE(1 == i1);
                REQUIRE(2 == i2);
                REQUIRE(3 == i3);
            }
        }

        AND_WHEN("I do a series of random enq and deq operations.") {
            q.enq(1);
            q.enq(2);
            q.enq(3);
            int i1 = *q.deq();
            q.enq(4);
            int i2 = *q.deq();
            q.enq(5);
            int i3 = *q.deq();
            q.enq(6);
            int i4 = *q.deq();
            q.enq(7);
            int i5 = *q.deq();
            q.enq(8);
            int i6 = *q.deq();

            THEN("All deq operations should be in order.") {
                REQUIRE(1 == i1);
                REQUIRE(2 == i2);
                REQUIRE(3 == i3);
                REQUIRE(4 == i4);
                REQUIRE(5 == i5);
                REQUIRE(6 == i6);
            }
        }
    }
}

SCENARIO("Static circular queue exceptions.") {

    GIVEN("I have a static circular queue.") {
        StaticCQueue<int, 3> q;

        WHEN("I insert 4 elements.") {
            q.enq(1);
            q.enq(2);
            q.enq(3);

            THEN("I should get an overflow error on the 4th insert.") {
                REQUIRE_THROWS_AS(q.enq(4), std::overflow_error);
            }
        }

        AND_WHEN("I do a deq operation in an empty Q.") {

            THEN("I should get back a nullopt.") {
                REQUIRE(std::nullopt == q.deq());
            }
        }

        AND_WHEN("I do a random set of inserts and deletes with more deletes.") {

            q.enq(1);
            q.enq(2);
            q.enq(3);
            q.deq();
            q.enq(4);
            q.deq();
            q.enq(5);
            q.deq();
            q.enq(6);
            q.deq();
            q.deq();
            q.deq();

            THEN("I should get back a nullopt.") {
                REQUIRE(std::nullopt == q.deq());
            }
            
        }
    }
}

SCENARIO("Static circular queue multi threaded inserts into the queue.") {
    constexpr static const std::size_t Q_SZ = 100;
    constexpr static const std::size_t THREADS = 10;
    constexpr static const std::size_t OPS_PER_THD = 10;

    GIVEN("I have a static circular queue.") {
        StaticCQueue<int, Q_SZ> q;

        WHEN("I do parallel inserts into it") {
            auto inputGen = []()-> int {
                return (int) rand();
            };
            auto insert = [&](const int& val) {
                // Add a random jitter
                std::this_thread::sleep_for(std::chrono::milliseconds(rand()%4));
                q.enq(val);
            };
            Benchmark<void, int> bm {insert, inputGen};
            bm.benchmarkWithoutResults(THREADS, OPS_PER_THD);

            THEN("I should have appropriate number of elements in the queue.") {
                REQUIRE(THREADS*OPS_PER_THD == q.size());
            }
        }

        WHEN("I do random inserts into the queue and record the inputs") {
            std::mutex lock;
            std::vector<int> vals;
            std::vector<int> reads;
            auto inputGen = []()-> int {
                return (int) rand();
            };
            auto insert = [&](const int& val) { 
                lock.lock();
                vals.push_back(val);
                lock.unlock();
                // Add a random jitter
                std::this_thread::sleep_for(std::chrono::milliseconds(rand()%4));
                q.enq(val);
            };
            auto read = [&](const int& dummy) {
                int val = *q.deq();
                std::this_thread::sleep_for(std::chrono::milliseconds(rand()%4));

                lock.lock();
                reads.push_back(val);
                lock.unlock();
            };

            Benchmark<void, int> bm {insert, inputGen};
            bm.benchmarkWithoutResults(THREADS, OPS_PER_THD);
            
            AND_WHEN("I read everything off of the queue and record the results.") {
                Benchmark<void, int> bm {read, inputGen};
                bm.benchmarkWithoutResults(THREADS, OPS_PER_THD);

                THEN("All the elements that were pushed should be there in the reads.") {
                    for (int& elem : vals) {
                        REQUIRE(std::find(reads.begin(), reads.end(), elem) != reads.end());
                    }
                }
            }
        }
    }
}

SCENARIO("Random mix of reads and writes.") {

    GIVEN("I have a static circular queue.") {
        StaticCQueue<int, 50> q;

        AND_GIVEN("I have 2 writers and a reader.") {
            constexpr static const std::size_t INPUTS_PER_WRITER = 100;
            constexpr static const std::size_t NUM_WRITERS = 2;
            std::vector<int> inputs1;
            std::vector<int> inputs2;
            std::vector<int> outputs;

            for (std::size_t i = 0; i < INPUTS_PER_WRITER; i++) {
                inputs1.push_back((int)rand());
                inputs2.push_back((int)rand());
            }
            auto writerThread = [&](const std::vector<int>& inputs) {
                for (int elem: inputs) {
                    while(true) {
                        try {
                            q.enq(elem);
                        } catch (const std::overflow_error& e) {
                            // Random sleep and retry on overflow error.
                            std::this_thread::sleep_for(std::chrono::milliseconds(rand()%4));
                            continue;
                        }
                        break;
                    }
                }
            };
            
            auto readerThread = [&]() {
                for (;outputs.size() < NUM_WRITERS*INPUTS_PER_WRITER;) {
                    while(true) {
                        std::optional<int> val = q.deq();
                        if (val.has_value()) {
                            outputs.push_back(*val);
                            break;
                        }
                    }
                }
            };

            WHEN("I have 2 threads push elements into the q and 1 reader.") {
                auto writer1 = std::thread(writerThread, inputs1);
                auto writer2 = std::thread(writerThread, inputs2);
                auto reader = std::thread(readerThread);

                writer1.join();
                writer2.join();
                reader.join();

                THEN("Finally, all outputs should match the inputs.") {
                    // Combine all the inputs
                    inputs1.insert(inputs1.end(), inputs2.begin(), inputs2.end());

                    // Sort the combined inputs and outputs to get a 1:1 correspondence between the elements
                    std::sort(inputs1.begin(), inputs1.end());
                    std::sort(outputs.begin(), outputs.end());

                    // Now compare each element
                    for(std::size_t i = 0; i < outputs.size(); i++) {
                        REQUIRE(inputs1[i] == outputs[i]);
                    }
                }
            }
        }
    }
}

