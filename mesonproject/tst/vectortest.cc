#define CATCH_CONFIG_MAIN

#include <vector>
#include <ds/vector.hpp>
#include <testing/catch.hpp>
#include <testing/benchmark.hpp>

using Sigabrt::Testing::randomInputGenerator;
using Sigabrt::DataStructures::Vector;

SCENARIO("Testing vector for single threaded operations.") {

    GIVEN("I Have a random sample vector") {
        std::vector<int> sample {randomInputGenerator(1000, 1, 1000)};

        WHEN("I initialize a Vecor from the sample std::vector.") {
            Vector<int> dut {sample};

            THEN("It should contain the same elements") {
                for (size_t idx = 0; idx < sample.size(); idx++) {
                    REQUIRE(sample[idx] == dut.get(idx));
                }
            }
        }
        
        AND_WHEN("I do a bunch of push and pops on a fresh Vector.") {
            Vector<int> dut {2};
            for (const auto& elem: sample) {
                dut.push(elem);
            }
            
            THEN("It should have the correct elements in correct places") {
                REQUIRE(dut.size() == sample.size());
                
                for(size_t idx = 0; idx < dut.size(); idx++) {
                    REQUIRE(sample[idx] == dut.get(idx));
                }
                
                for(size_t idx = dut.size(); idx > 0; idx--) {
                    REQUIRE(sample[idx-1] == dut.last());
                    REQUIRE(sample[idx-1] == dut.pop());
                }
            }
            
        }
    }
}
