#include <iostream>

#include <quadstore/quadstore.hpp>

using semantic::core::QuadStore;

int main() {
    QuadStore qs;
    qs.insert_triple({1,2,1,4});
    qs.insert_triple({1,2,2,4});
    qs.insert_triple({1,2,3,4});
    qs.insert_triple({1,3,1,4});
    qs.insert_triple({1,3,2,4});
    qs.insert_triple({1,3,3,4});
    qs.insert_triple({1,4,1,4});
    qs.insert_triple({1,4,2,4});
    qs.insert_triple({1,4,3,4});
    qs.insert_triple({1,5,1,4});

    qs.insert_triple({2,2,1,4});
    qs.insert_triple({2,2,2,4});
    qs.insert_triple({2,2,3,4});

    std::cout << "\n-----------------------------------------------------\n";
}
