#define CATCH_CONFIG_MAIN

#include <catch2/catch_all.hpp>

#include <algorithm>
#include <vector>
#include <ranges>
#include <iterator>
#include <cstdlib>
#include <unordered_set>

#include <iostream>

#include <quadstore/models.hpp>
#include <quadstore/quadstore.hpp>
#include <thesoup/types/types.hpp>

using std::views::filter;
using semantic::core::QuadStore;
using semantic::models::Triple;
using semantic::models::ErrorCode;
using semantic::models::QueryFilter;
using thesoup::types::Result;
using thesoup::types::Unit;


SCENARIO("Quad store inserts.") {

    GIVEN("I have a quad store with and i insert a bunch of triples.") {

        QuadStore qs;
        std::vector<Triple> sample_triples {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
                {1,1,2,2},
                {1,2,1,1},
                {1,2,1,2},
                {1,2,2,1},
                {1,2,2,2},
                {2,1,1,1},
                {2,1,1,2},
                {2,1,2,1},
                {2,1,2,2},
        };

        std::vector<Triple> duplicates {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
        };

        std::vector<Triple> all {};

        // combine the 2
        all.insert(
            all.end(),
            duplicates.begin(),
            duplicates.end()
        );

        all.insert(
                all.end(),
                sample_triples.begin(),
                sample_triples.end()
        );

        std::vector<Result<Unit,ErrorCode>> insert_results;
        std::transform(
                all.begin(),
                all.end(),
                std::back_inserter(insert_results),
                [&](const Triple& t){return qs.insert_triple(t);}
        );

        WHEN("I query the size of the quad store.") {

            THEN("It should be same as the size of the.") {

                REQUIRE(sample_triples.size() == qs.size());

                AND_THEN("All inserts should have succeeded, except the duplicate ones.") {

                    auto success_it {insert_results | filter([&](const Result<Unit,ErrorCode>& elem) -> bool { return elem;})};
                    auto fail_it {insert_results | filter([&](const Result<Unit,ErrorCode>& elem) -> bool { return !elem;})};
                    REQUIRE(sample_triples.size() == static_cast<std::uint64_t>(std::abs(std::ranges::distance(success_it.begin(), success_it.end()))));
                    REQUIRE(duplicates.size() == static_cast<std::uint64_t>(std::abs(std::ranges::distance(fail_it.begin(), fail_it.end()))));

                }
            }
        }
    }
}

SCENARIO("Quad store queries.") {

    GIVEN("I have a quad store with and i insert a bunch of triples.") {

        QuadStore qs;
        std::vector<Triple> sample_triples {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
                {1,1,2,2},
                {1,2,1,1},
                {1,2,1,2},
                {1,2,2,1},
                {1,2,2,2},
                {1,5,5,1},


                {2,1,1,1},
                {2,1,1,2},
                {2,1,2,1},
                {2,1,2,2},
        };

        std::vector<Triple> duplicates {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
        };

        std::vector<Triple> all {};

        // combine the 2
        all.insert(
                all.end(),
                duplicates.begin(),
                duplicates.end()
        );

        all.insert(
                all.end(),
                sample_triples.begin(),
                sample_triples.end()
        );

        std::vector<Result<Unit,ErrorCode>> insert_results;
        std::transform(
                all.begin(),
                all.end(),
                std::back_inserter(insert_results),
                [&](const Triple& t){return qs.insert_triple(t);}
        );

        WHEN("I query without a namespace.") {

            auto res {qs.query({std::nullopt,1,2,3})};
            REQUIRE(!res);
            REQUIRE(res.error() == ErrorCode::UNSPECIFIED_NAMESPACE);
        }

        WHEN("I query by namespace only.") {

            auto res {qs.query({1, std::nullopt, std::nullopt, std::nullopt})};

            THEN("I should get an error stating that either subject or the object need to be specified.") {

                REQUIRE(!res);
                REQUIRE(res.error() == ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
            }
        }

        WHEN("I query by namespace and pred only.") {

            auto res {qs.query({1, std::nullopt, 1, std::nullopt})};

            THEN("I should get an error stating that either subject or the object need to be specified.") {

                REQUIRE(!res);
                REQUIRE(res.error() == ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
            }
        }

        WHEN("I query with everything specified for a triple that does exist.") {

            auto res {qs.query({1, 1, 1, 2})};

            THEN("I should get back the 1 correct triple.") {

                REQUIRE(res);

                REQUIRE(res.unwrap().size() == 1);
                REQUIRE(res.unwrap()[0] == Triple{1,1,1,2});
            }
        }

        WHEN("I query by subject only.") {

            auto res {qs.query({1, 1, std::nullopt, std::nullopt})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);

                REQUIRE(res.unwrap().size() == 4);

                auto subj_1_it {all | filter([&](const Triple& elem) -> bool { return elem.context==1 && elem.subj==1;})};
                std::vector<Triple> expected_triples {};
                std::copy(subj_1_it.begin(), subj_1_it.end(), std::back_inserter(expected_triples));

                for (const Triple& item : expected_triples) {
                    REQUIRE(std::find(res.unwrap().begin(), res.unwrap().end(), item) != res.unwrap().end());
                }
            }

        }

        WHEN("I query by subject and predicate.") {

            auto res {qs.query({1, 1, 1, std::nullopt})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);

                REQUIRE(res.unwrap().size() == 2);

                auto subj_1_it {all | filter([&](const Triple& elem) -> bool { return elem.context==1 && elem.subj==1 && elem.pred==1;})};
                std::vector<Triple> expected_triples {};
                std::copy(subj_1_it.begin(), subj_1_it.end(), std::back_inserter(expected_triples));

                for (const Triple& item : res.unwrap()) {
                    REQUIRE(std::find(res.unwrap().begin(), res.unwrap().end(), item) != res.unwrap().end());
                }
            }

        }

        WHEN("I query by object only.") {

            auto res {qs.query({1, std::nullopt, std::nullopt, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);

                REQUIRE(res.unwrap().size() == 5);

                auto subj_1_it {all | filter([&](const Triple& elem) -> bool { return elem.context==1 && elem.obj==1;})};
                std::vector<Triple> expected_triples {};
                std::copy(subj_1_it.begin(), subj_1_it.end(), std::back_inserter(expected_triples));

                for (const Triple& item : expected_triples) {
                    REQUIRE(std::find(res.unwrap().begin(), res.unwrap().end(), item) != res.unwrap().end());
                }
            }

        }

        WHEN("I query by object and predicate.") {

            auto res {qs.query({1, std::nullopt, 5, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap().size() == 1);
                REQUIRE(res.unwrap()[0] == Triple{1,5,5,1});
            }

        }

        WHEN("I query by subject and object") {

            auto res {qs.query({1, 1, std::nullopt, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap().size() == 2);
                std::vector<Triple> expected_triples {
                        {1,1,1,1},
                        {1,1,2,1},
                };

                std::for_each(
                        expected_triples.begin(),
                        expected_triples.end(),
                        [&](const Triple& t) {
                            REQUIRE(std::find(res.unwrap().begin(), res.unwrap().end(), t) != res.unwrap().end());
                        }
                );
            }
        }

    }
}

SCENARIO("Quad store deletes.") {

    GIVEN("I have a quad store with and i insert a bunch of triples.") {

        QuadStore qs;
        std::vector<Triple> sample_triples {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
                {1,1,2,2},
                {1,2,1,1},
                {1,2,1,2},
                {1,2,2,1},
                {1,2,2,2},
                {1,5,5,1},


                {2,1,1,1},
                {2,1,1,2},
                {2,1,2,1},
                {2,1,2,2},
        };

        std::vector<Triple> duplicates {
                {1,1,1,1},
                {1,1,1,2},
                {1,1,2,1},
        };

        std::vector<Triple> all {};

        // combine the 2
        all.insert(
                all.end(),
                duplicates.begin(),
                duplicates.end()
        );

        all.insert(
                all.end(),
                sample_triples.begin(),
                sample_triples.end()
        );

        std::vector<Result<Unit,ErrorCode>> insert_results;
        std::transform(
                all.begin(),
                all.end(),
                std::back_inserter(insert_results),
                [&](const Triple& t){return qs.insert_triple(t);}
        );

        WHEN("I delete without a namespace.") {

            auto res {qs.delete_triples({std::nullopt,1,2,3})};
            REQUIRE(!res);
            REQUIRE(res.error() == ErrorCode::UNSPECIFIED_NAMESPACE);
        }

        WHEN("I delete by namespace only.") {

            auto res {qs.delete_triples({1, std::nullopt, std::nullopt, std::nullopt})};

            THEN("I should get an error stating that either subject or the object need to be specified.") {

                REQUIRE(!res);
                REQUIRE(res.error() == ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
            }
        }

        WHEN("I delete by namespace and pred only.") {

            auto res {qs.delete_triples({1, std::nullopt, 1, std::nullopt})};

            THEN("I should get an error stating that either subject or the object need to be specified.") {

                REQUIRE(!res);
                REQUIRE(res.error() == ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
            }
        }

        WHEN("I delete with everything specified for a triple that does exist.") {

            std::uint64_t sz {qs.size()};
            auto res {qs.delete_triples({1, 1, 1, 2})};

            THEN("I should have deleted just 1 triple and that should not be there anymore.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 1);
                REQUIRE(qs.size() == sz-1);
                REQUIRE(qs.query({1, 1, 1, 2}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, 1, 1, 2}).unwrap() == 0);
            }
        }

        WHEN("I delete by subject only.") {

            auto res {qs.delete_triples({1, 1, std::nullopt, std::nullopt})};

            THEN("I should have deleted the correct number of triples.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 4);
                REQUIRE(qs.query({1, 1, std::nullopt, std::nullopt}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, 1, std::nullopt, std::nullopt}).unwrap() == 0);
            }

        }

        WHEN("I delete by subject and predicate.") {

            auto res {qs.delete_triples({1, 1, 1, std::nullopt})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 2);
                REQUIRE(qs.query({1, 1, 1, std::nullopt}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, 1, 1, std::nullopt}).unwrap() == 0);
            }

        }

        WHEN("I delete by object only.") {

            REQUIRE(qs.query({1, std::nullopt, std::nullopt, 1}).unwrap().size() == 5);
            auto res {qs.delete_triples({1, std::nullopt, std::nullopt, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 5);
                REQUIRE(qs.query({1, std::nullopt, std::nullopt, 1}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, std::nullopt, std::nullopt, 1}).unwrap() == 0);
            }

        }

        WHEN("I delete by object and predicate.") {

            REQUIRE(qs.query({1, std::nullopt, 5, 1}) == 1);
            auto res {qs.delete_triples({1, std::nullopt, 5, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 1);
                REQUIRE(qs.query({1, std::nullopt, 5, 1}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, std::nullopt, 5, 1}).unwrap() == 0);
            }

        }

        WHEN("I delete by subject and object") {

            REQUIRE(qs.query({1, 1, std::nullopt, 1}).unwrap().size() == 2);
            auto res {qs.delete_triples({1, 1, std::nullopt, 1})};

            THEN("I should get back the correct triple, with the inputted subject ID.") {

                REQUIRE(res);
                REQUIRE(res.unwrap() == 2);
                REQUIRE(qs.query({1, 1, std::nullopt, 1}).unwrap().size() == 0);
                REQUIRE(qs.delete_triples({1, 1, std::nullopt, 1}).unwrap() == 0);
            }
        }

    }
}
