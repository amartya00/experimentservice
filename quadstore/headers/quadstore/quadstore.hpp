#pragma once

#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <thesoup/types/types.hpp>

#include <quadstore/models.hpp>

namespace semantic {

    namespace core {

        class QuadStore {
            using index_by_all = boost::multi_index::hashed_unique<
                    boost::multi_index::tag<struct composite_tag>,
                    boost::multi_index::composite_key<
                        semantic::models::Triple,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::context>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::subj>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::pred>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::obj>
                    >
            >;

            using index_by_sub = boost::multi_index::hashed_non_unique<
                    boost::multi_index::tag<struct subj_tag>,
                    boost::multi_index::composite_key<
                            semantic::models::Triple,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::context>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::subj>
                    >
            >;

            using index_by_pred = boost::multi_index::hashed_non_unique<
                    boost::multi_index::tag<struct pred_tag>,
                    boost::multi_index::composite_key<
                            semantic::models::Triple,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::context>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::pred>
                    >
            >;

            using index_by_obj = boost::multi_index::hashed_non_unique<
                    boost::multi_index::tag<struct obj_tag>,
                    boost::multi_index::composite_key<
                            semantic::models::Triple,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::context>,
                            boost::multi_index::member<semantic::models::Triple, std::uint64_t,&semantic::models::Triple::obj>
                    >
            >;

            using QuadMap = boost::multi_index_container<
                    semantic::models::Triple,
                    boost::multi_index::indexed_by<
                        index_by_all,
                        index_by_sub,
                        index_by_pred,
                        index_by_obj
                    >
            >;

        private:
            QuadMap store;
        public:
            QuadStore();
            std::uint64_t size()const noexcept;
            thesoup::types::Result<thesoup::types::Unit, semantic::models::ErrorCode> insert_triple(const models::Triple& t) noexcept;
            thesoup::types::Result<std::vector<semantic::models::Triple>, semantic::models::ErrorCode> query(const semantic::models::QueryFilter& query_filter) const noexcept;
            thesoup::types::Result<std::uint64_t, semantic::models::ErrorCode> delete_triples(const semantic::models::QueryFilter& filter) noexcept;
        };
    }
}