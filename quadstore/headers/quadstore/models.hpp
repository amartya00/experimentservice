#pragma once

#include <cstdint>
#include <optional>

namespace semantic {

    namespace models {

        struct Triple {
            std::uint64_t context;
            std::uint64_t subj;
            std::uint64_t pred;
            std::uint64_t obj;
        };

        enum class ErrorCode {
            UNKNOWN_ERROR,
            UNSPECIFIED_NAMESPACE,
            NO_SUB_OR_OBJ_SPECIFIED,
            INSERT_FAILURE
        };

        struct QueryFilter {
            std::optional<std::uint64_t> context_id;
            std::optional<std::uint64_t> subj_id;
            std::optional<std::uint64_t> pred_id;
            std::optional<std::uint64_t> obj_id;
        };

        uint64_t generate_triple_id(const Triple& t) noexcept;

        bool operator==(const Triple& l, const Triple& r);
    }

}