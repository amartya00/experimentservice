#include <algorithm>
#include <vector>
#include <optional>
#include <tuple>

#include <thesoup/types/types.hpp>

#include <quadstore/quadstore.hpp>

using semantic::core::QuadStore;

using semantic::models::Triple;
using semantic::models::ErrorCode;
using semantic::models::QueryFilter;
using semantic::models::generate_triple_id;
using semantic::core::subj_tag;
using semantic::core::obj_tag;

using thesoup::types::Result;
using thesoup::types::Unit;


Result<std::vector<Triple>, ErrorCode> QuadStore::query(const semantic::models::QueryFilter &query_filter) const noexcept {
    // A null namespace is error
    // TODO: Very ugly code. See if it can be made pretty
    if (query_filter.context_id == std::nullopt) {
        return Result<std::vector<Triple>, ErrorCode>::failure(ErrorCode::UNSPECIFIED_NAMESPACE);
    } else if (query_filter.subj_id == std::nullopt && query_filter.obj_id == std::nullopt) {
        return Result<std::vector<Triple>, ErrorCode>::failure(ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
    } else if (query_filter.subj_id != std::nullopt && query_filter.obj_id != std::nullopt && query_filter.pred_id != std::nullopt) {
        std::vector<Triple> retval;
        auto [begin, end] {
            store.get<composite_tag>().equal_range(
                std::make_tuple(*query_filter.context_id, *query_filter.subj_id, *query_filter.pred_id, *query_filter.obj_id)
            )
        };
        std::copy(begin, end, std::back_inserter(retval));
        return Result<std::vector<Triple>, ErrorCode>::success(std::move(retval));

    } else if (query_filter.subj_id != std::nullopt) {
        // Cases where subject ID is not null
        auto [begin, end] {store.get<subj_tag>().equal_range(std::make_tuple(*query_filter.context_id, *query_filter.subj_id))};
        std::vector<Triple> retval;

        if (query_filter.pred_id == std::nullopt && query_filter.obj_id == std::nullopt) {
            // case 1: Both pred and obj are null
            std::copy(begin,end,std::back_inserter(retval));
        } else if (query_filter.pred_id != std::nullopt && query_filter.obj_id == std::nullopt) {
            // Case 2: Pred is not null but Obj is null
            std::copy_if(
                    begin,
                    end,
                    std::back_inserter(retval),
                    [&](const Triple& t) {
                        return t.pred == *query_filter.pred_id;
                    }
            );
        } else {
            // Case 3: Pred is null but Obj is not
            std::copy_if(
                    begin,
                    end,
                    std::back_inserter(retval),
                    [&](const Triple& t) {
                        return t.obj == *query_filter.obj_id;
                    }
            );
        }
        return Result<std::vector<Triple>, ErrorCode>::success(std::move(retval));
    } else {
        // Cases where subject ID is null
        // Case 1: Both pred and object are null: ILLEGAL and covered already
        // Case 2: Pred ID is not null but obj is null: Illegal and covered already.
        auto [begin, end] {store.get<obj_tag>().equal_range(std::make_tuple(*query_filter.context_id, *query_filter.obj_id))};
        std::vector<Triple> retval;

        if (query_filter.pred_id == std::nullopt) {
            // Case 3: Pred ID is null
            std::copy(
                    begin,
                    end,
                    std::back_inserter(retval)
            );
        } else {
            // Case 4: Pred ID is not null
            std::copy_if(
                    begin,
                    end,
                    std::back_inserter(retval),
                    [&](const Triple& t) {
                        return t.pred == *query_filter.pred_id;
                    }
            );
        }
        return Result<std::vector<Triple>, ErrorCode>::success(std::move(retval));
    }
}