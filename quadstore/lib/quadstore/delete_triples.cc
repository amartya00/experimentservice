#include <ranges>
#include <optional>
#include <tuple>
#include <cstdlib>

#include <iostream>

#include <boost/iterator/filter_iterator.hpp>

#include <quadstore/quadstore.hpp>
#include <thesoup/types/types.hpp>


using std::views::filter;
using boost::make_filter_iterator;
using semantic::core::QuadStore;
using thesoup::types::Result;
using thesoup::types::Unit;
using semantic::models::Triple;
using semantic::models::ErrorCode;
using semantic::models::QueryFilter;
using semantic::core::QuadStore;

inline bool _null_or_equal(const std::optional<std::uint64_t> filter_param, const std::uint64_t val) {
    return filter_param == std::nullopt || *filter_param == val;
}

Result<std::uint64_t, ErrorCode> QuadStore::delete_triples(const QueryFilter& f) noexcept {
    if (f.context_id == std::nullopt) {
        return Result<std::uint64_t, ErrorCode>::failure(ErrorCode::UNSPECIFIED_NAMESPACE);
    } else if(f.subj_id == std::nullopt && f.obj_id == std::nullopt) {
        return Result<std::uint64_t, ErrorCode>::failure(ErrorCode::NO_SUB_OR_OBJ_SPECIFIED);
    } else if (f.subj_id != std::nullopt && f.pred_id != std::nullopt && f.obj_id != std::nullopt) {
        // Case 0: (sob, pred, obj)
        auto it {store.get<composite_tag>().find(std::make_tuple(*f.context_id, *f.subj_id, *f.pred_id, *f.obj_id))};
        std::uint64_t deleted_items {0};
        if (it != store.get<composite_tag>().end()) {
            store.get<composite_tag>().erase(it);
            deleted_items++;
        }
        return Result<std::uint64_t, ErrorCode>::success(std::move(deleted_items));
    } else if (f.subj_id != std::nullopt) {
        std::uint64_t items_deleted {0};
        if (f.pred_id == std::nullopt && f.obj_id == std::nullopt) {
            // Case 1: (sub, NULL, NULL)
            auto [begin, end] {store.get<subj_tag>().equal_range(std::make_tuple(*f.context_id, *f.subj_id))};
            items_deleted = static_cast<std::uint64_t>(std::abs(std::distance(begin, end)));
            store.get<subj_tag>().erase(begin, end);
        } else if(f.pred_id != std::nullopt && f.obj_id == std::nullopt) {
            // Case 2: (sub, pred, NULL)
            auto [begin, end] {store.get<subj_tag>().equal_range(std::make_tuple(*f.context_id, *f.subj_id))};
            auto it {begin};
            while (it != end) {
                if (it->pred == *f.pred_id) {
                    it = store.get<subj_tag>().erase(it);
                    items_deleted++;
                } else {
                    it++;
                }
            }
        } else if(f.pred_id == std::nullopt && f.obj_id != std::nullopt) {
            // Case 3: (sub, NULL, obj)
            auto [begin, end] {store.get<subj_tag>().equal_range(std::make_tuple(*f.context_id, *f.subj_id))};
            auto it {begin};
            while (it != end) {
                if (it->obj == *f.obj_id) {
                    it = store.get<subj_tag>().erase(it);
                    items_deleted++;
                } else {
                    it++;
                }
            }
        }
        return Result<std::uint64_t, ErrorCode>::success(std::move(items_deleted));
    } else {
        std::uint64_t items_deleted {0};
        if (f.pred_id != std::nullopt && f.obj_id != std::nullopt) {
            // Case 4: (NULL, pred, obj)
            auto [begin, end] {store.get<obj_tag>().equal_range(std::make_tuple(*f.context_id, *f.obj_id))};
            auto it {begin};
            while (it != end) {
                if (it->pred == *f.pred_id) {
                    it = store.get<obj_tag>().erase(it);
                    items_deleted++;
                } else {
                    it++;
                }
            }
        } else {
            // Case 5: (NULL, NULL, obj)
            // TODO: Remains to be seen if I want to allow this.
            auto [begin, end] {store.get<obj_tag>().equal_range(std::make_tuple(*f.context_id, *f.obj_id))};
            items_deleted = static_cast<std::uint64_t>(std::abs(std::distance(begin, end)));
            store.get<obj_tag>().erase(begin, end);
        }
        return Result<std::uint64_t, ErrorCode>::success(std::move(items_deleted));
    }

}