#include <quadstore/quadstore.hpp>

using semantic::core::QuadStore;


QuadStore::QuadStore() {}

std::uint64_t QuadStore::size() const noexcept {
    return static_cast<std::uint64_t >(store.size());
}
