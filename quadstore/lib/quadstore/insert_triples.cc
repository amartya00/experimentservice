#include <thesoup/types/types.hpp>

#include <quadstore/quadstore.hpp>

using semantic::core::QuadStore;
using semantic::models::Triple;
using semantic::models::ErrorCode;

using thesoup::types::Result;
using thesoup::types::Unit;


Result<Unit,ErrorCode> QuadStore::insert_triple(const models::Triple &t) noexcept {
    const auto& [_it, res] = store.insert(t);
    return res ? Result<Unit,ErrorCode>::success(Unit::unit) : Result<Unit,ErrorCode>::failure(ErrorCode::INSERT_FAILURE);
}
