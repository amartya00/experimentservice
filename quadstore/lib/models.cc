#include <quadstore/models.hpp>

using semantic::models::Triple;

std::uint64_t semantic::models::generate_triple_id(const Triple& t) noexcept {
    auto const h1 {std::hash<std::uint64_t>()(t.context)};
    auto const h2 {std::hash<std::uint64_t>()(t.context)};
    auto const h3 {std::hash<std::uint64_t>()(t.context)};
    auto const h4 {std::hash<std::uint64_t>()(t.context)};

    return h1 ^ (h2 << 1) ^ (h3 << 2) ^ (h4 << 3);
}

bool semantic::models::operator==(const Triple& l, const Triple& r) {
    return l.context == r.context && l.subj == r.subj && l.pred == r.pred && l.obj == r.obj;
}