use std::convert::TryFrom;

use mysql::{Pool, PooledConn, Value};
use chrono::NaiveDateTime;

use super::models::TaskStatus;

pub struct TasksDao {
    pub(crate) conn: PooledConn,
    pub(crate) table_name: String
}


impl TasksDao {
    pub fn new(
        user: &str,
        password: &str,
        host: &str,
        port: u64,
        database_name: &str,
        tasks_table_name: &str
    ) -> TasksDao {
        let url = format!("mysql://{user}:{password}@{host}:{port}/{database_name}");
        let pool = Pool::new(url.as_str()).unwrap();
        let conn = pool.get_conn().unwrap();
        TasksDao {
            conn,
            table_name: String::from(tasks_table_name)
        }
    }

    // TODO: Investigate if the Error type can be more efficient than String
    pub (crate) fn sql_val_to_native_date(v: Value) -> Result<Option<NaiveDateTime>, String> {
        match v {
            Value::NULL => Ok(None),
            Value::Bytes(b) => {
                    NaiveDateTime::parse_from_str(
                        String::from_utf8(b).unwrap().as_str(),
                        "%Y-%m-%d %H:%M:%S"
                    )
                        .map(|res| { Some(res) })
                        .map_err(|e| {e.to_string()})
            }
            Value::Date(y, m, d, h, mm,s, _) => {
                NaiveDateTime::parse_from_str(
                    format!("{y}-{m}-{d} {h}:{mm}:{s}").as_str(),
                    "%Y-%m-%d %H:%M:%S"
                )
                    .map(|res| {Some(res)})
                    .map_err(|e| {e.to_string()})
            }
            _ => Err(format!("Invalid type to try and convert to native datetime. In file {}, line {}", file!(), line!()))
        }
    }

    pub (crate) fn sql_val_to_task_status(v: Value) -> Result<TaskStatus, String> {
        match v {
            Value::UInt(val) => TaskStatus::from_uint(val)
                .map_err(|_| { format!("Failed to convert value to TaskStatus. File {}, line {}", file!(), line!()) }),
            Value::Int(val) => u64::try_from(val)
                .map_err(|e| {e.to_string()})
                .and_then(|v| {
                    TaskStatus::from_uint(v)
                        .map_err(|_| {format!("Could not convert value to Task status. File {}, line {}", file!(), line!())})}
                ),
            Value::Bytes(b) => String::from_utf8(b)
                .map_err(|e| {e.to_string()})
                .and_then(
                    |v| {
                        v.parse::<u64>().map_err(|_| {format!("Failed to parse '{}' to u64, File {}, Line {}", v, file!(), line!())})
                    }
                )
                .and_then(
                    |v| {
                        TaskStatus::from_uint(v).map_err(
                            |_| {
                                format!("Could not convert to a valid TaskStatus. File {}, line {}", file!(), line!())
                            }
                        )
                    }
                ),

            _ => Err(format!("Invalid type to try and convert to TaskStatus. In file {}, line {}", file!(), line!()))
        }
    }
}
