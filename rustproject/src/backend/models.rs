use chrono::NaiveDateTime;
use std::fmt;
use std::fmt::Formatter;

pub enum TaskStatus {
    Pending,
    InProgress,
    Complete
}

impl TaskStatus {
    pub fn to_uint(&self) -> u64 {
        match &self {
            TaskStatus::Pending => 0,
            TaskStatus::InProgress => 1,
            TaskStatus::Complete => 2
        }
    }

    pub fn from_uint(val: u64) -> Result<TaskStatus, ()> {
        match val {
            0 => Ok(TaskStatus::Pending),
            1 => Ok(TaskStatus::InProgress),
            2 => Ok(TaskStatus::Complete),
            _ => Err(())
        }
    }
}

impl fmt::Display for TaskStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            TaskStatus::Pending => {write!(f, "PENDING")}
            TaskStatus::InProgress => {write!(f, "IN_PROGRESS")}
            TaskStatus::Complete => {write!(f, "COMPLETE")}
        }
    }
}

pub struct Task {
    pub task_id: u64,
    pub title: String,
    pub description: Option<String>,
    pub status: TaskStatus,
    pub due_by: Option<NaiveDateTime>,
    pub created_at: NaiveDateTime
}

impl Task {
    pub const TASK_FIELDS: [&'static str; 6] = [
        "task_id",
        "title",
        "description",
        "status",
        "due_date",
        "created_at"
    ];
}

impl fmt::Display for Task {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let description = match &self.description {
            None => {String::from("NULL")}
            Some(val) => {String::from(val)}
        };
        let due_by = match &self.due_by {
            None => {String::from("NEVER")}
            Some(dt) => {String::from(format!("{dt}"))}
        };

        let ff = format!(
            "(id={}, title='{}', description='{}',status={}, due_by={}, created_at={})",
            &self.task_id,
            &self.title,
            description,
            &self.status,
            due_by,
            &self.created_at
        );
        write!(f, "{ff}")
    }
}