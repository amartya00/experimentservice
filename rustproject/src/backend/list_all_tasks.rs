use super::dao::TasksDao;
use super::models::Task;

use std::convert::TryFrom;

use mysql::prelude::*;
use chrono::NaiveDateTime;
use mysql::Value;

impl TasksDao {
    pub fn list_all(&mut self) -> Vec<Task> {
        let query_str = format!(
            r#"
            SELECT {} FROM {}
            "#,
            Task::TASK_FIELDS.join(","),
            self.table_name
        );

        self.conn.query_map(
            query_str,
            |(task_id,title,description,status,due_date,created_at)| {
                let parsed_due_by: Option<NaiveDateTime> = TasksDao::sql_val_to_native_date(due_date).unwrap();
                let parsed_created_at: NaiveDateTime = TasksDao::sql_val_to_native_date(created_at).unwrap().unwrap();
                let parsed_status = TasksDao::sql_val_to_task_status(status).unwrap();
                Task {
                    task_id, title, description, status: parsed_status, due_by: parsed_due_by, created_at: parsed_created_at
                }
            }
        ).unwrap()
    }

    pub fn num_tasks(&mut self) -> Result<u64, String> {
        let query_str = format!(
            r#"
            SELECT COUNT(*) FROM {}
            "#,
            self.table_name
        );
        let r: Vec<Result<u64, String>> = self.conn.query_map(
            query_str,
            |v: Value| {
                match v {
                    Value::UInt(val) => Ok::<u64, String>(val),
                    Value::Int(val) => u64::try_from(val)
                        .map_err(|e| {e.to_string()}),
                    Value::Bytes(b) => String::from_utf8(b)
                        .map_err(|e| {e.to_string()})
                        .and_then(
                            |v| {
                                v.parse::<u64>().map_err(|_| {format!("Failed to parse '{}' to u64, File {}, Line {}", v, file!(), line!())})
                            }
                        ),
                    _ => Err(format!("Invalid type to try and convert to TaskStatus. In file {}, line {}", file!(), line!()))
                }
            }
        ).unwrap();
            //.map(|v| {v[0]});
        Err("ss".to_string())

    }
}