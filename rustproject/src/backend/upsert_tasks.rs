use super::dao::TasksDao;
use super::models::Task;

use mysql::prelude::*;
use std::collections::HashMap;


impl TasksDao {
    #[inline]
    fn convert_task_to_dict(task: &Task) -> HashMap<&str, String> {
        let mut object_dict = HashMap::<&str,String>::new();
        let desc = task.description.as_ref()
            .map(|v| {format!("'{v}'")})
            .unwrap_or("NULL".to_string());

        let due_by = task.due_by.as_ref()
            .map(|d| {format!("'{}'", d.to_string())})
            .unwrap_or("NULL".to_string());

        object_dict.insert("task_id", task.task_id.to_string());
        object_dict.insert("title", format!("'{}'",&task.title));
        object_dict.insert("description", desc);
        object_dict.insert("status", task.status.to_uint().to_string());
        object_dict.insert("due_date", due_by);
        object_dict.insert("created_at", format!("'{}'", task.created_at.to_string()));
        object_dict
    }

    pub fn upsert_task(&mut self, task: &Task) -> Result<(), ()> {
        let dict = TasksDao::convert_task_to_dict(&task);
        let collision_str = dict
            .iter()
            .map(|(k, v)|{format!("{}={}",k,v)})
            .collect::<Vec<String>>()
            .join(",");

        let mut insert_vals = Vec::<&str>::new();
        for x in Task::TASK_FIELDS {
            insert_vals.push(dict[x].as_str());
        }
        let insert_str = insert_vals.join(",");
        let sql = format!(
            r#"
            INSERT INTO {} ({}) VALUES
                ({})
            ON DUPLICATE KEY UPDATE
                {}
            "#,
            self.table_name,
            Task::TASK_FIELDS.join(","),
            insert_str,
            collision_str
        );

        println!(" Executing following sql: \n {sql}");
        self.conn.query(sql).map(|_: Vec<String>| {()}).map_err(|_| {{}})
    }
}