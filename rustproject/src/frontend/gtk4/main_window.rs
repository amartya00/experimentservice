use std::cell::RefCell;
use std::rc::Rc;
use gtk4::prelude::*;
use gtk4::Application;

use super::super::super::backend::dao::TasksDao;


pub struct MainWindow {
    app: Application,
    pub(crate) dao: Rc<RefCell<TasksDao>>
}

impl MainWindow {
    pub fn new(app_id: &str, mut dao: Rc<RefCell<TasksDao>>) -> MainWindow {
        MainWindow {
            app: Application::builder().application_id(app_id).build(),
            dao
        }
    }

    pub fn start(&mut self) -> i32 {
        self.app.connect_activate(
            |app: &Application| {
                self.build_ui(app)
            }
        );
        self.app.run()
    }
}