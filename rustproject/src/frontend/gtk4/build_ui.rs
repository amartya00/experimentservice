use std::cell::RefCell;
use std::rc::Rc;
use gtk4::prelude::*;
use gtk4::{Application, ApplicationWindow,Button};
use crate::TasksDao;

use super::super::super::frontend::gtk4::main_window::MainWindow;

impl MainWindow {
    pub(crate) fn build_ui(&mut self, app: &Application) {
        let button = Button::builder()
            .label("Press me!")
            .margin_top(12)
            .margin_bottom(12)
            .margin_start(12)
            .margin_end(12)
            .build();

        button.connect_clicked(
            |b| {
                let num_tasks = (*self.dao).borrow_mut().num_tasks().unwrap();
                b.set_label(format!("Clicked!!! Num tasks = {}", num_tasks).as_str());
            }
        );

        let window = ApplicationWindow::builder()
            .application(app)
            .default_width(320)
            .default_height(200)
            .title("My GTK App")
            .child(&button)
            .build();

        window.present();
    }
}