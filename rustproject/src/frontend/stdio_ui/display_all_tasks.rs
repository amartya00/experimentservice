use chrono::NaiveDateTime;
use crate::backend::models::Task;
use super::super::super::backend::dao::*;


fn task_to_string(task: &Task) -> String {

    format!(
        r#"
[Task id {}] {}

{}

due by: {}

------


        "#,
        &task.task_id,
        &task.title,
        match &task.description {
            None => "",
            Some(val) => val.as_str()
        },
        match &task.due_by.map(|d: NaiveDateTime| {d.to_string()}) {
            None => "NEVER",
            Some(val) => val.as_str()
        }
    )
}

pub fn display_all_tasks(dao: &mut TasksDao) {
    println!("Tasks:\n--------------------------\n\n");
    dao.list_all().iter().for_each(
        |item: &Task| {
            println!("{}", task_to_string(item));
        }
    );
    println!("\n\nFinished...\n\n");
}