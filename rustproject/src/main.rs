pub mod backend;
pub mod frontend;
pub mod configuration;

use std::cell::RefCell;
use std::rc::Rc;
use chrono::NaiveDateTime;
use backend::dao::TasksDao;
use frontend::stdio_ui::display_all_tasks::display_all_tasks;
use frontend::gtk4::main_window::MainWindow;
use configuration::app_config::AppConfig;
use crate::backend::models::{Task, TaskStatus};

fn main() {
    const APP_ID: &str = "org.gtk_rs.HelloWorld1";

    let config = AppConfig::load();
    let user = config.user.as_str();
    let pwd = config.pwd.as_str();
    let host = config.host.as_str();
    let database_name = config.database.as_str();

    let mut dao = TasksDao::new(
        user,
        pwd,
        host,
        3306,
        database_name,
        "tasks"
    );

    display_all_tasks(&mut dao);
    let mut window = MainWindow::new(APP_ID, Rc::new(RefCell::new(dao)));
    window.start::<'static>();
}