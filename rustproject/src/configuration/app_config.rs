use serde::{Deserialize, Serialize};
use std::{env, fs, io, path::PathBuf};
use std::io::{Read, Write};
use std::fs::File;

#[derive(Serialize, Deserialize)]
pub struct AppConfig {
    pub host: String,
    pub database: String,
    pub tasks_table: String,
    pub user: String,
    pub pwd: String
}

impl AppConfig {
    fn config_dir() -> PathBuf {
        let mut buff = PathBuf::new();
        buff.push(env::var("HOME").unwrap());
        buff.push(".helper");
        buff
    }

    fn config_file() -> PathBuf {
        AppConfig::config_dir().join("config.json")
    }

    fn create_paths_if_needed() {
        // Does data folder exist?
        let config_dir = AppConfig::config_dir();
        let config_path = AppConfig::config_file();

        if !config_dir.is_dir() {
            println!("Config dir does not exist creating one...");
            fs::create_dir(config_dir.as_path()).expect("Failed to create data folder ...");
        }

        // Does config file exist?
        if !config_path.is_file() {
            println!("Config file does not exist, creating one ...");
            AppConfig::reconfigure(&config_path);
        }
    }

    fn construct_config_from_input() -> AppConfig {
        fn input(prompt: &str) -> String {
            print!("{prompt}");
            io::stdout().flush().unwrap();
            let mut res = String::new();
            io::stdin()
                .read_line(&mut res)
                .expect("Failed to read line");
            res.trim().to_string()
        }

        let db_name = "helper";
        let tasks_table_name = "tasks";
        let host = input("Enter host: ");
        let user = input("Enter user name: ");
        let pwd = input("Enter password: ");

        AppConfig {
            host,
            database: String::from(db_name),
            tasks_table: String::from(tasks_table_name),
            user,
            pwd
        }
    }

    pub fn load() -> AppConfig {
        AppConfig::create_paths_if_needed();
        println!("Reading config ...");
        let mut f = File::open(AppConfig::config_file()).expect("Failed to open config file");
        let mut data = String::new();
        f.read_to_string(&mut data).expect("Failed to read config file ...");
        serde_json::from_str(&data).expect("Failed to parse json into config")
    }

    pub fn reconfigure(config_path: &PathBuf) -> AppConfig {
        let conf = AppConfig::construct_config_from_input();
        let serialized = serde_json::to_string(&conf).unwrap();
        let mut f = File::create(config_path).expect("Failed to create conf file");
        f.write_all(serialized.as_bytes()).expect("Failed to write to config file ...");
        f.flush().unwrap();
        conf
    }
}
